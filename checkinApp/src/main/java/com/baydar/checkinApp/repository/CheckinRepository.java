package com.baydar.checkinApp.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.baydar.checkinApp.model.Checkin;

@Repository
public interface CheckinRepository extends JpaRepository<Checkin, Long> {
	Page<Checkin> findByUserId(Long userId, Pageable pageable);
}
