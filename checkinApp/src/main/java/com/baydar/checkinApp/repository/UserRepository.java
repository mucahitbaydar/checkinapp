package com.baydar.checkinApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.baydar.checkinApp.model.User;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

}
