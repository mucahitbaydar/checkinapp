package com.baydar.checkinApp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.baydar.checkinApp.model.Place;

@Repository
public interface PlaceRepository extends JpaRepository<Place, Long> {

}
