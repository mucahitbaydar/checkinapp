package com.baydar.checkinApp.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.baydar.checkinApp.model.Category;
import com.baydar.checkinApp.repository.CategoryRepository;

@RestController
public class CategoryController {

	@Autowired
	private CategoryRepository categoryRepository;

	@GetMapping("/categories")
	public Page<Category> getAllCategories(Pageable pageable) {
		return categoryRepository.findAll(pageable);
	}

	@PostMapping("/categories/{categoryId}")
	public Category createCategory(@PathVariable(value = "categoryId") Long categoryId,
			@Valid @RequestBody Category category) {
		return categoryRepository.save(category);
	}

	@PutMapping("/categories/{categoryId}")
	public Category updateCategory(@PathVariable(value = "categoryId") Long categoryId,
			 @Valid @RequestBody Category categoryRequest) {
		if (!categoryRepository.existsById(categoryId)) {
			throw new ResourceNotFoundException("CategoryId " + categoryId + " not found");
		}

		return categoryRepository.findById(categoryId).map(category -> {
			category.setName(categoryRequest.getName());
			return categoryRepository.save(category);
		}).orElseThrow(() -> new ResourceNotFoundException("CategoryId " + categoryId + "not found"));
	}

	@DeleteMapping("/categories/{categoryId}")
	public ResponseEntity<?> deleteCategory(@PathVariable(value = "categoryId") Long categoryId) {
		return categoryRepository.findById(categoryId).map(category -> {
			categoryRepository.delete(category);
			return ResponseEntity.ok().build();
		}).orElseThrow(() -> new ResourceNotFoundException("CategoryId " + categoryId + " not found"));
	}

}
