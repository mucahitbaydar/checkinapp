package com.baydar.checkinApp.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.baydar.checkinApp.model.Checkin;
import com.baydar.checkinApp.model.Place;
import com.baydar.checkinApp.model.User;
import com.baydar.checkinApp.repository.CheckinRepository;
import com.baydar.checkinApp.repository.PlaceRepository;
import com.baydar.checkinApp.repository.UserRepository;

@RestController
public class CheckinController {
	@Autowired
	private CheckinRepository checkinRepository;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private PlaceRepository placeRepository;

	@GetMapping("/users/{userId}/checkins")
	public Page<Checkin> getAllCheckinsByUserId(@PathVariable(value = "userId") Long userId, Pageable pageable) {
		return checkinRepository.findByUserId(userId, pageable);
	}

	@GetMapping("/checkins")
	public Page<Checkin> getAllCheckins(Pageable pageable) {
		return checkinRepository.findAll(pageable);
	}

	@PostMapping("/users/{userId}/checkins/{placeId}")
	public Checkin createComment(@PathVariable(value = "userId") Long userId,
			@PathVariable(value = "placeId") Long placeId, @Valid @RequestBody Checkin checkin) {

		if (!placeRepository.existsById(placeId)) {
			throw new ResourceNotFoundException("PlaceId " + placeId + " not found");
		}

		return userRepository.findById(userId).map(user -> {
			checkin.setUser(user);
			user.setNum_checkins(user.getNum_checkins() + 1);
			Place place = placeRepository.getOne(placeId);
			place.setNum_checkins(place.getNum_checkins() + 1);
			place.getCategory().setNum_checkins(place.getCategory().getNum_checkins() + 1);
			checkin.setPlace(place);
			return checkinRepository.save(checkin);
		}).orElseThrow(() -> new ResourceNotFoundException("UserId " + userId + " not found"));
	}

	@PutMapping("/users/{userId}/checkins/{checkinId}/places/{placeId}")
	public Checkin updateComment(@PathVariable(value = "userId") Long userId,
			@PathVariable(value = "placeId") Long placeId, @PathVariable(value = "checkinId") Long checkinId,
			@Valid @RequestBody Checkin checkinRequest) {
		if (!userRepository.existsById(userId)) {
			throw new ResourceNotFoundException("UserId " + userId + " not found");
		}

		if (!placeRepository.existsById(placeId)) {
			throw new ResourceNotFoundException("PlaceId " + placeId + " not found");
		}

		return checkinRepository.findById(checkinId).map(checkin -> {
			checkin.setPlaceName(checkinRequest.getPlaceName());
			checkin.setPlace(placeRepository.getOne(placeId));
			return checkinRepository.save(checkin);
		}).orElseThrow(() -> new ResourceNotFoundException("CheckinId " + checkinId + "not found"));
	}

	@DeleteMapping("/users/{userId}/checkins/{checkinId}")
	public ResponseEntity<?> deleteComment(@PathVariable(value = "userId") Long userId,
			@PathVariable(value = "checkinId") Long checkinId) {
		if (!userRepository.existsById(userId)) {
			throw new ResourceNotFoundException("UserId " + userId + " not found");
		}

		return checkinRepository.findById(checkinId).map(checkin -> {
			checkin.getPlace().setNum_checkins(checkin.getPlace().getNum_checkins() - 1);
			checkin.getPlace().getCategory().setNum_checkins(checkin.getPlace().getCategory().getNum_checkins() - 1);
			User user = userRepository.getOne(userId);
			user.setNum_checkins(user.getNum_checkins() - 1);
			checkinRepository.delete(checkin);
			return ResponseEntity.ok().build();
		}).orElseThrow(() -> new ResourceNotFoundException("CheckinId " + checkinId + " not found"));
	}

}
