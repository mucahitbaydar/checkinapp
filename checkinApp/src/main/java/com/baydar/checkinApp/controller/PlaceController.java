package com.baydar.checkinApp.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.baydar.checkinApp.model.Place;
import com.baydar.checkinApp.repository.CategoryRepository;
import com.baydar.checkinApp.repository.PlaceRepository;

@RestController
public class PlaceController {
	@Autowired
	private PlaceRepository placeRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	

	@GetMapping("/places")
	public Page<Place> getAllPlaces(Pageable pageable) {
		return placeRepository.findAll(pageable);
	}

	@PostMapping("/places/{categoryId}")
	public Place createPlace(@PathVariable(value = "categoryId") Long categoryId, @Valid @RequestBody Place place) {
		if (!categoryRepository.existsById(categoryId)) {
			throw new ResourceNotFoundException("CategoryId " + categoryId + " not found");
		}
		place.setCategory(categoryRepository.getOne(categoryId));
		return placeRepository.save(place);
	}

	@PutMapping("/places/{placeId}")
	public Place updatePlace(@PathVariable(value = "placeId") Long placeId, @Valid @RequestBody Place placeRequest) {
		if (!placeRepository.existsById(placeId)) {
			throw new ResourceNotFoundException("PlaceId " + placeId + " not found");
		}

		return placeRepository.findById(placeId).map(place -> {
			place.setName(placeRequest.getName());
			return placeRepository.save(place);
		}).orElseThrow(() -> new ResourceNotFoundException("PlaceId " + placeId + "not found"));
	}

	@DeleteMapping("/places/{placeId}")
	public ResponseEntity<?> deletePlace(@PathVariable(value = "placeId") Long placeId) {
		return placeRepository.findById(placeId).map(place -> {
			placeRepository.delete(place);
			return ResponseEntity.ok().build();
		}).orElseThrow(() -> new ResourceNotFoundException("PlaceId " + placeId + " not found"));
	}
}
